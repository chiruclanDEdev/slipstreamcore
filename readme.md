= SlipstreamCore 4.0.6a Mmaps Version-- General information =

Copyright (C) SlipstreamCore  (http://slipstreamcore.chiruclan.de)
Copyright (C) SkyFireEMU      (http://www.projectskyfire.org)
Copyright (C) Cactus          (http://www.cactusemu.com)
Copyright (C) TrinityCore     (http://www.trinitycore.org)
Copyright (C) MaNGOS          (http://www.getmangos.com)


  SlipstreamCore is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation version 3 of the License.
  In addition, as a special exception, the SlipstreamCore project
  gives permission to link the code of its release of SlipstreamCore with
  the OpenSSL project's "OpenSSL" library (or with modified versions of
  it that use the same license as the "OpenSSL" library), and distribute
  the linked executables.  You must obey the GNU General Public License
  in all respects for all of the code used other than "OpenSSL".  If you
  modify this file, you may extend this exception to your version of the
  file, but you are not obligated to do so.  If you do not wish to do
  so, delete this exception statement from your version.


SlipstreamCore is a MMORPG Framework based mostly on C++. It is completely 
open source, and is community supported. It is derived
from SkyFireEMU, CactusCore, TrinityCore, and MaNGOS, the Massive Network Game Object Servers, 
and is based on the code of there projects with extensive changes over time to optimize, 
improve and cleanup the codebase at the same time as improving the ingame mechanics
and functionality. If you wish to contribute ideas or code please visit 
our site linked below or make pull requests to our bitbucket repo at 
https://bitbucket.org/Chiruclan/slipstreamcore

## Requirements

+ Platform: Linux, Windows or Mac
+ Processor with SSE2 support
+ ACE = 5.8.3 (included for Windows)
+ MySQL = 5.1.0 (included for Windows)
+ CMake = 2.8.0
+ OpenSSL = 0.9.8o
+ GCC = 4.3 (Linux only)
+ MS Visual Studio = 9 (2008) (Windows only)


## Install

Detailed installation guides are available in the wiki for
[Windows] (http://wiki.projectskyfire.org/index.php?title=Installation_Windows),
[Linux] (http://wiki.projectskyfire.org/index.php?title=Installation_Linux) and
[Mac OSX] (http://wiki.projectskyfire.org/index.php?title=Installation_Mac_OS_X).


## Reporting issues

Issues can be reported via the [Github issue tracker](https://github.com/ProjectSkyfire/SkyFireEMU/issues).

Please take the time to review existing issues before submitting your own to
prevent duplicates.

## Submitting fixes

Fixes are submitted as pull requests via Github. 

## Copyright

License: GPL 3.0

Read file [COPYING](COPYING.md)


## Authors &amp; Contributors

Read file [THANKS](https://github.com/ProjectSkyfire/SkyFireEMU/tree/master/doc/THANKS.md)


## Links

Forum [http://www.projectskyfire.org](http://www.projectskyfire.org)

Database [http://www.projectskyfire.org/index.php?/files/] (http://www.projectskyfire.org/index.php?/files/)

Wiki [http://wiki.projectskyfire.org](http://wiki.projectskyfire.org)
